import 'package:flutter/material.dart';

//Necessário para conectar com a API
import 'package:http/http.dart' as http;
//Converter para json
import 'dart:convert';

const _request = "https://api.hgbrasil.com/finance?key=cb3a40f5";

void main() async {
  //Transformando a string em map
  //print(json.decode(response.body));
  //Navegando pelos resultados
  //print(json.decode(response.body)["results"]["currencies"]["USD"]);

  runApp(
    MaterialApp(
        home: Home(),
        theme: ThemeData(
          //TEMA DO APP
          hintColor: Colors.amber,
          primaryColor: Colors.white,
          // inputDecorationTheme: InputDecorationTheme(
          //enabledBorder:
          //    OutlineInputBorder(borderSide: BorderSide(color: Colors.white)),
          // focusedBorder:
          //     OutlineInputBorder(borderSide: BorderSide(color: Colors.amber)),
          //  hintStyle: TextStyle(color: Colors.amber),
        )),
  );
}

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

TextEditingController realController = TextEditingController();
TextEditingController dolarController = TextEditingController();
TextEditingController euroController = TextEditingController();

class _HomeState extends State<Home> {
  void cleanAll() {
    realController.clear();
    dolarController.clear();
    euroController.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
          backgroundColor: Colors.black,
          appBar: AppBar(
            actions: [
              IconButton(
                  icon: Icon(Icons.refresh),
                  onPressed: () {
                    cleanAll();
                  })
            ],
            centerTitle: true,
            title: Text(
              "\$ Conversor \$",
              style: TextStyle(color: Colors.white),
            ),
            backgroundColor: Colors.amber,
          ),
          body: converterScreen()),
    );
  }
}

Widget converterScreen() {
  //Loading enquanto carrega dados é feito pelo futureBuilder

  return FutureBuilder(
    builder: (context, snapshot) {
      switch (snapshot.connectionState) {
        case ConnectionState.none:
        case ConnectionState.waiting:
          return loadingScreen();
          break;
        default:
          if (snapshot.hasError) {
            return errorScreen();
          } else {
            return mainScreen(
              dolar: snapshot.data["results"]["currencies"]["USD"]["buy"],
              euro: snapshot.data["results"]["currencies"]["EUR"]["buy"],
            );
          }

          break;
      }
    },
    future: apiCall(),
  );
}

Widget mainScreen({double dolar, double euro}) {
  void _realChanged(String text) {
    if (realController.text.isNotEmpty) {
      double real = double.parse(text);
      dolarController.text = (real / dolar).toStringAsFixed(2);
      euroController.text = (real / euro).toStringAsFixed(2);
    } else {
      dolarController.clear();
      euroController.clear();
    }
  }

  void _dolarChanged(String text) {
    if (dolarController.text.isNotEmpty) {
      double dolarText = double.parse(text);
      realController.text = (dolar * dolarText).toStringAsFixed(2);
      euroController.text = ((dolar * dolarText) / euro).toStringAsFixed(2);
    } else {
      realController.clear();
      euroController.clear();
    }
  }

  void _euroChanged(String text) {
    if (euroController.text.isNotEmpty) {
      double euroText = double.parse(text);
      realController.text = (euroText * euro).toStringAsFixed(2);
      dolarController.text = ((euro * euroText) / dolar).toStringAsFixed(2);
    } else {
      dolarController.clear();
      euroController.clear();
    }
  }

  return SingleChildScrollView(
    padding: EdgeInsets.all(10),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Icon(
          Icons.monetization_on,
          size: 150,
          color: Colors.amber,
        ),
        customTextField("R\$", "Reais", realController, _realChanged),
        customTextField("US\$", "Dolar", dolarController, _dolarChanged),
        customTextField("€", "Euros", euroController, _euroChanged),
      ],
    ),
  );
}

Widget customTextField(
    String prefix, String label, TextEditingController controller, Function f) {
  return Padding(
    padding: EdgeInsets.only(bottom: 10, top: 10),
    child: TextField(
      keyboardType: TextInputType.number,
      onChanged: f,
      controller: controller,
      style: TextStyle(color: Colors.amber, fontSize: 25),
      decoration: InputDecoration(
          hintStyle: TextStyle(color: Colors.white),
          prefixText: prefix,
          focusedBorder:
              OutlineInputBorder(borderSide: BorderSide(color: Colors.amber)),
          enabledBorder:
              OutlineInputBorder(borderSide: BorderSide(color: Colors.amber)),
          labelText: label,
          labelStyle: TextStyle(color: Colors.amber)),
    ),
  );
}

Widget errorScreen() {
  return Center(
    child: Text(
      "Erro ao carregar carregados :(",
      style: TextStyle(color: Colors.amber, fontSize: 25),
      textAlign: TextAlign.center,
    ),
  );
}

Widget loadingScreen() {
  return Center(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        CircularProgressIndicator(
          backgroundColor: Colors.grey,
          valueColor: new AlwaysStoppedAnimation<Color>(Colors.amberAccent),
        ),
        Padding(
          padding: EdgeInsets.all(50),
          child: Text(
            "Carregando dados.",
            style: TextStyle(color: Colors.amber, fontSize: 25),
            textAlign: TextAlign.center,
          ),
        ),
      ],
    ),
  );
}

//Chamada async da API
Future<Map> apiCall() async {
  http.Response response = await http.get(_request);
  return json.decode(response.body);
}
