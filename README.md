# Currency Converter

An implementation of an application in Flutter that converts currencies using data obtained through an API. It is an example to show how to use the HTTP request and Json data.

Obs 1: The http package is not included by default, it is necessary to put the plugin in pubspec.yaml

  dependencies:   
 
    flutter:
 
    sdk: flutter
 
    cupertino_icons: ^0.1.2
 
    http: ^0.12.0+2        # ADD THIS LINE IN PUBSPEC FILE


# Sample screens
![enter image description here](https://gitlab.com/ariltonJAguilar/currency-converter/-/raw/master/samples/sample1.jpg)
![enter image description here](https://gitlab.com/ariltonJAguilar/currency-converter/-/raw/master/samples/sample2.jpg)
